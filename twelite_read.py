from serial import *
from sys import stdout, stdin, stderr, exit

class Twelite():
    def __init__(self, port):
        try:
            self.ser = Serial(port, 115200)
            print "open serial port: %s" % port
        except:
            print "cannot open serial port: %s" % port
            return


    def read(self):
        while True:
            line = self.ser.readline().rstrip()
            read_list = map(ord, line[1:].decode('hex'))
            if sum(read_list) & 0xff == 0:
                print read_list
            else:
                print "checksum failed"
                continue
